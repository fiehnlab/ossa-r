---
title: "Using OSSA for R"
author: "Gert Wohlgemuth"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

OSSA for R is the REST based implementation of the OpenCl Spectral Similarity Search tool, which allows you to search on mass spectral data on a remote or local server, while utilizing GPU's for the actual search. This allows for similarity search in the range of milli seconds instead of seconds.

You can find the server and C++ code at: https://bitbucket.org/DantesInferno/spectrumsimilarity-dante